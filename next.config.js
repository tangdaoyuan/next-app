const path = require('path')
const withTypescript = require("@zeit/next-typescript")
const withSass = require("@zeit/next-sass")
const withLess = require('@zeit/next-less')
const withCss = require('@zeit/next-css')
const withLessExcludeAntd = require("./config/next.less.config.js")

if (typeof require !== 'undefined') {
  require.extensions['.less'] = file => {}
  require.extensions['.css'] = file => {}
}

const runtimeConfig = {
  serverRuntimeConfig: {
    secret: 'server',
  },
  publicRuntimeConfig: {
    secret: 'public'
  }
}

module.exports = {
  distDir: 'build',
  ...runtimeConfig,
  ...withTypescript(
    withLessExcludeAntd(
      withSass(
        withCss({
          webpack: config => {
            return config
          },
          cssLoaderOptions: {
            importLoaders: 1,
            localIdentName: "[local]___[hash:base64:5]",
          },
          lessLoaderOptions: {
            javascriptEnabled: true,
          },
          cssModules: true
        })
      )
    )
)};