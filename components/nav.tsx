import React from 'react'
import Link from 'next/link'
import './nav.scss'

const links = [
  {
    href: 'https://github.com/segmentio/create-next-app', 
    label: 'Github',
    key: '',
    as: 'https://github.com/segmentio/create-next-app'
  }, {
    href: '/about?name=Query come from Client',
    label: 'About',
    key: '',
    as: '/about'
  }
].map(link => {
  link['key'] = `nav-link-${link.href}-${link.label}`
  return link
})

class Nav extends React.Component<{}, {}> {
  public render () {
    return (
      <nav>
          <ul>
            <li>
              <Link prefetch href="/">
                <a>Home</a>
              </Link>
            </li>
            <ul>
              {links.map(({ key, href, label, as }) => (
                <li key={key}>
                  <Link href={href} as={as}>
                    <a>{label}</a>
                  </Link>
                </li>
              ))}
            </ul>
          </ul>
        </nav>
    )
  }
}

export default Nav
