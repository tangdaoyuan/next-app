import React from 'react'
import Head from '../components/head'
import { Card } from 'antd'
import '../assets/styles.scss'

interface AboutProps {
  name ?: string
}

class About extends React.Component<AboutProps, {}> {

  public static getInitialProps({ query }: {query: any}) {
    if (query.name) {
      return {name: query.name}
    }
    return {}
  }

  public render () {
    return (
      <div className="about">
        <Head title="about"></Head>
        <Card title="Ant Card" >
          <p>{this.props.name}</p>
        </Card>
      </div>
    )
  }
}

export default About