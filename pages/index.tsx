import React from 'react'
import Link from 'next/link'
import getConfig from 'next/config'
import Head from '../components/head'
import Nav from '../components/nav'
import '../assets/styles.scss'


const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

console.log(serverRuntimeConfig.secret)
console.log(publicRuntimeConfig.secret)

interface HomeProps {
  userAgent: any
}

class Home extends React.Component<HomeProps, {}> {

   /*
   * For the initial page load, 
   * getInitialProps will execute on the server only. 
   * getInitialProps will only be executed on the client when navigating to a different route via the Link component or using the routing APIs.
   */
  public static async getInitialProps({ req }: { req: any }) {
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
    return { userAgent }
  }

  public render () {
    return (
      <div>
        <Head title="Home" />
        <Nav />
        <div className="hero">
          <h1 className="title">Welcome to Next!</h1>
          <p className="description">
            To get started, edit <code>pages/index.js</code> and save to reload.
          </p>

          <div className="row">
            <Link href={{
              protocol: 'https', 
              hostname: 'github.com',
              pathname: '/zeit/next.js',
              hash: 'getting-started'}}>
              <a className="card">
                <h3>Getting Started &rarr;</h3>
                <p>Learn more about Next on Github and in their examples</p>
              </a>
            </Link>
            <Link href="https://open.segment.com/create-next-app">
              <a className="card">
                <h3>Examples &rarr;</h3>
                <p>
                  Find other example boilerplates on the{' '}
                  <code>create-next-app</code> site
                </p>
              </a>
            </Link>
            <Link href="https://github.com/segmentio/create-next-app">
              <a className="card">
                <h3>Create Next App &rarr;</h3>
                <p>Was this tool helpful? Let us know how we can improve it</p>
              </a>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

export default Home